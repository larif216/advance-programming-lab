package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    protected FlyBehavior flyBehavior;
    protected QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public abstract void display();

    public void swim() {
        System.out.println("All ducks float, even decoys!");
    }

    public void setFlyBehavior(FlyBehavior fh) {
        this.flyBehavior = fh;
    }

    public void setQuackBehavior(QuackBehavior qh) {
        this.quackBehavior = qh;
    }
}
