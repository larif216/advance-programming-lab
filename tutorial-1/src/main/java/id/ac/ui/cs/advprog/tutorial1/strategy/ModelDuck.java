package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
        this.flyBehavior = new FlyNoWay();
        this.quackBehavior = new MuteQuack();
    }

    @Override
    public void display() {
        System.out.println("I am Model Duck");
    }
}
