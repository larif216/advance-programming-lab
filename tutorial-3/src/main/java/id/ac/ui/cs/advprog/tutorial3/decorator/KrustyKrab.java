package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class KrustyKrab {

    public static void main(String[] args) {
        Food patty = new ThickBunBurger();
        System.out.println(patty.getDescription() + " $" + patty.cost());

        Food krabbyPatty = BreadProducer.THICK_BUN.createBreadToBeFilled();
        krabbyPatty = FillingDecorator.BEEF_MEAT.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.CHEESE.addFillingToBread(krabbyPatty);
        krabbyPatty = FillingDecorator.CHILI_SAUCE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription() + " $" + krabbyPatty.cost());
    }
}
