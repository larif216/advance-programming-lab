package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class MyCompany {

    public static void main(String[] args) {
        Company myCompany = new Company();

        Employees ceo = new Ceo("A", 200000);
        Employees cto = new Cto("B", 100000);
        Employees backend = new BackendProgrammer("C", 20000);
        Employees frontend = new FrontendProgrammer("D", 30000);
        Employees networkExpert = new NetworkExpert("E", 50000);
        Employees securityExpert = new SecurityExpert("F", 70000);
        Employees uiUxDesigner = new UiUxDesigner("G", 90000);

        myCompany.addEmployee(ceo);
        myCompany.addEmployee(cto);
        myCompany.addEmployee(backend);
        myCompany.addEmployee(frontend);
        myCompany.addEmployee(networkExpert);
        myCompany.addEmployee(securityExpert);
        myCompany.addEmployee(uiUxDesigner);

        System.out.println("My Company's team: ");
        for (Employees employee: myCompany.getAllEmployees()) {
            System.out.format("%s : %s with salary %.2f\n",
                    employee.getRole(), employee.getName(), employee.getSalary());
        }

        System.out.println("The team's netted salary: " + myCompany.getNetSalaries());
    }
}
