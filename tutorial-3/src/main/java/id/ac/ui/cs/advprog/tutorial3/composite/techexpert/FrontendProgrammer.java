package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.minSalary = 30000;
        this.setSalary(salary);
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
