package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary) {
        this.name = name;
        this.minSalary = 70000;
        this.setSalary(salary);
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
