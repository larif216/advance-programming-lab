package id.ac.ui.cs.advprog.tutorial3.composite;

public abstract class Employees {
    protected String name = "Unidentified Name";
    protected double salary;
    protected String role;
    protected double minSalary;

    public String getName() {
        return this.name;
    }

    public abstract double getSalary();

    public String getRole() {
        return this.role;
    }

    public void setSalary(double salary) {
        if (salary < minSalary) {
            throw new IllegalArgumentException();
        } else this.salary = salary;
    }
}
